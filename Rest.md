# **REST Architecture**

## **Introduction**


   Representational State Transfer (REST) was created by computer scientist **Roy Fielding** in 2000. REST is basically a style, a design, we can say that is something which is not a software but its just there as a style you can follow. Like we don't have a formal document, just a style that we need to follow.
  <br><br>
  So technically, REST is a type of an architecture widely used to create and design web services. So when you use this architecture in designing your API's, they are called **Restful**.
 
<br>

 ## **Architectural Concepts**
<br>

 ![image](https://miro.medium.com/max/1000/1*EbBD6IXvf3o-YegUvRB_IA.jpeg)

<br>

REST is basically designed for network-based application like client-server applications. Client requests a resource using a URL and server reponds with various resources.

<br>

 ## **Principles of REST**
 <br>

  **1. Client-Server Architecture :** A computer which provides you services is called as server. Clients are those request for the resources to server. So client initial request for service and server provides that service.
 <br><br>


 ![image](https://miro.medium.com/max/468/1*_Bc3-FKtaV--gfh2QftNrg.png)
 <br>
<br>

  **2. Statelessness :** Statelessness means the server treats each HTTP request as independent request and does not contain history of any previous request.
<br>

  **3. Cacheability :**  An HTTP response is cacheable when it is stored on a device to retrive and use it later.
 <br>

 **4. Use of layered system :** A layered system is a system which divides the design into small pieces. This is done to handle different processes at different levels. 
 <br> 


  **5. Uniform interface :**  REST uses HTTP for transfer protocol so every service needs to use the HTTP interface the same way, which has fixed four operations - GET, POST, PUT, DELETE. These are used to read, update, create, and delete resources. So this should be same across the internet.
 <br>


**6. Code on demand :** The ability of a server to send the right code to the client when it is requested.
 <br>

 <br>

 ### These principles must be followed to make any web service - a true RESTful API.
 <br>

 ## **Properties of REST**
 <br>

 **1. Performance :** Performance is in terms of solving a particular problem immdiately by one click and get quick results.
 <br>

 **2. Scalability :** Scalability in terms of web is to provide consistent information (service) regardless of increase or decrease of web server users.
<br>

**3. Simplicity:** Simplicity is that functionality the ensures each component is less complex and easy to understand and implement. 
<br>

 **4. Visibility :** It refers the ability of a component to interact with other components. Visibility enables performance, scalability, reliability, security.
<br>

**5. Portability :** It ensures the ability of a software to run properly in different platforms with little or no modifications.
<br>


## **What is API ?**
<br>

API is simply a messenger that transfers data from one location to another location, which allows two or more applications to talk with each other. 
<br>

<br>

![image](https://blog.axway.com/wp-content/uploads/2019/03/WHAT-IS-AN-API-PHOTO-1.png)
 <br><br>

## **What is REST API ?**
<br>
The API which is developed using REST is known as REST API / RESTful API. It uses HTTP interface which has fixed four operations - GET, POST, PUT, DELETE. These are used to read, update, create, and delete resources. So this should be same across the internet.
<br>

<br>

![image](https://voximplant.com/assets/images/2020/11/27/rest-api-model.png)
 <br><br>
## **References**
<br>

  - [https://en.wikipedia.org/wiki/Representational_state_transfer](https://en.wikipedia.org/wiki/Representational_state_transfer)
  <br>

  - [https://www.youtube.com/watch?v=madENMEfHLg](https://www.youtube.com/watch?v=madENMEfHLg)
  <br>

  - [https://www.youtube.com/watch?v=Jzv3G5iDLvw](https://www.youtube.com/watch?v=Jzv3G5iDLvw)
  <br>
  
  - [https://restfulapi.net/rest-architectural-constraints/](https://restfulapi.net/rest-architectural-constraints/)